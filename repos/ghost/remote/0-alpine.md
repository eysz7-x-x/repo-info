## `ghost:0-alpine`

```console
$ docker pull ghost@sha256:f5a040e0b298bb2d4eae49a36d24d6251affcd00a2367dbd824bb092457ac834
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `ghost:0-alpine` - linux; amd64

```console
$ docker pull ghost@sha256:021a357235e4951724530c61f2bcbc42386830665bce6683bbd54028757d89d3
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **46.5 MB (46451197 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:3b4338e27ee1017e37720965cc8003a45dd52cf752090235e7f022d1f1c12e88`
-	Entrypoint: `["docker-entrypoint.sh"]`
-	Default Command: `["npm","start"]`

```dockerfile
# Fri, 06 Jul 2018 14:15:48 GMT
ADD file:3861bd6a79d134647f9f78098d7f8a3612e6e848284806eea9b3ca4ca23d8686 in / 
# Fri, 06 Jul 2018 14:15:49 GMT
CMD ["/bin/sh"]
# Tue, 04 Sep 2018 22:06:30 GMT
ENV NODE_VERSION=6.14.4
# Tue, 04 Sep 2018 22:21:42 GMT
RUN addgroup -g 1000 node     && adduser -u 1000 -G node -s /bin/sh -D node     && apk add --no-cache         libstdc++     && apk add --no-cache --virtual .build-deps         binutils-gold         curl         g++         gcc         gnupg         libgcc         linux-headers         make         python   && for key in     94AE36675C464D64BAFA68DD7434390BDBE9B9C5     FD3A5288F042B6850C66B31F09FE44734EB7990E     71DCFD284A79C3B38668286BC97EC7A07EDE3FC1     DD8F2338BAE7501E3DD5AC78C273792F7D83545D     C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8     B9AE9905FFD7803F25714661B63B535A4C206CA9     56730D5401028683275BD23C23EFEFE93C4CFFFE     77984A986EBC2AA786BC0F66B01FBB92821C587A     8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600   ; do     gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" ||     gpg --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" ||     gpg --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ;   done     && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION.tar.xz"     && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc"     && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc     && grep " node-v$NODE_VERSION.tar.xz\$" SHASUMS256.txt | sha256sum -c -     && tar -xf "node-v$NODE_VERSION.tar.xz"     && cd "node-v$NODE_VERSION"     && ./configure     && make -j$(getconf _NPROCESSORS_ONLN)     && make install     && apk del .build-deps     && cd ..     && rm -Rf "node-v$NODE_VERSION"     && rm "node-v$NODE_VERSION.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt
# Tue, 04 Sep 2018 22:21:42 GMT
ENV YARN_VERSION=1.6.0
# Tue, 04 Sep 2018 22:21:46 GMT
RUN apk add --no-cache --virtual .build-deps-yarn curl gnupg tar   && for key in     6A010C5166006599AA17F08146C2130DFD2497F5   ; do     gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" ||     gpg --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" ||     gpg --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ;   done   && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz"   && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc"   && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz   && mkdir -p /opt   && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/   && ln -s /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn   && ln -s /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg   && rm yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz   && apk del .build-deps-yarn
# Tue, 04 Sep 2018 22:21:46 GMT
CMD ["node"]
# Wed, 05 Sep 2018 08:51:21 GMT
RUN apk add --no-cache 'su-exec>=0.2'
# Wed, 05 Sep 2018 08:51:22 GMT
RUN apk add --no-cache 		bash 		tar
# Wed, 05 Sep 2018 08:51:23 GMT
ENV GHOST_SOURCE=/usr/src/ghost
# Wed, 05 Sep 2018 08:51:23 GMT
WORKDIR /usr/src/ghost
# Wed, 05 Sep 2018 08:51:23 GMT
ENV GHOST_VERSION=0.11.13
# Wed, 05 Sep 2018 08:52:36 GMT
RUN set -ex; 		apk add --no-cache --virtual .build-deps 		ca-certificates 		gcc 		make 		openssl 		python 		unzip 	; 		wget -O ghost.zip "https://github.com/TryGhost/Ghost/releases/download/${GHOST_VERSION}/Ghost-${GHOST_VERSION}.zip"; 	unzip ghost.zip; 		npm install --production; 		apk del .build-deps; 		rm ghost.zip; 	npm cache clean; 	rm -rf /tmp/npm*
# Wed, 05 Sep 2018 08:52:37 GMT
ENV GHOST_CONTENT=/var/lib/ghost
# Wed, 05 Sep 2018 08:52:38 GMT
RUN mkdir -p "$GHOST_CONTENT" 	&& chown -R node:node "$GHOST_CONTENT" 	&& ln -s "$GHOST_CONTENT/config.js" "$GHOST_SOURCE/config.js"
# Wed, 05 Sep 2018 08:52:38 GMT
VOLUME [/var/lib/ghost]
# Wed, 05 Sep 2018 08:52:39 GMT
COPY file:2cb0a64ef22301242537372657c5d88304b43153f351a7f2d0d61e05c3dfb29a in /usr/local/bin/ 
# Wed, 05 Sep 2018 08:52:39 GMT
ENTRYPOINT ["docker-entrypoint.sh"]
# Wed, 05 Sep 2018 08:52:39 GMT
EXPOSE 2368/tcp
# Wed, 05 Sep 2018 08:52:39 GMT
CMD ["npm" "start"]
```

-	Layers:
	-	`sha256:4064ffdc82fe2f815e6146dcdb37e8ca22376aea17a5ec58d123fe7070377fb6`  
		Last Modified: Fri, 06 Jul 2018 14:18:31 GMT  
		Size: 2.4 MB (2387548 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:107cfce516860179b39f7ef96650b4deddd5faae68601dc214323c85033fc559`  
		Last Modified: Tue, 04 Sep 2018 22:52:42 GMT  
		Size: 15.5 MB (15516465 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:2315b442b9e6fb17b35b506b731e4696c82cb2712179faffe8ecdaa33cd55574`  
		Last Modified: Tue, 04 Sep 2018 22:52:38 GMT  
		Size: 1.1 MB (1080069 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9bfe2ad95cc685557506ed19f7b9e3296d7c5aec9eb15993f3e52524eda8eb33`  
		Last Modified: Wed, 05 Sep 2018 08:55:52 GMT  
		Size: 8.4 KB (8352 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:fc5156366366efa3369df7c2bf29f8582cf7a017003985ecaac505b2b9c0f415`  
		Last Modified: Wed, 05 Sep 2018 08:55:52 GMT  
		Size: 1.4 MB (1360673 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d246fb7da5872824c2d0288efd16dd2f137fe10b65a7f60a45eabd5513e804ed`  
		Last Modified: Wed, 05 Sep 2018 08:55:51 GMT  
		Size: 128.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:902ac304744f398ea6ba664607793dabc2b9781e73cee713349e83aa5f6c27dd`  
		Last Modified: Wed, 05 Sep 2018 08:55:58 GMT  
		Size: 26.1 MB (26097127 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:5c7dab81e687f95c86aa6e26db81e1cb7a9324e68f95402b348e18039757b595`  
		Last Modified: Wed, 05 Sep 2018 08:55:51 GMT  
		Size: 226.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:d9d90cccf25133feaf51396a1450954c8453ab39f7f28349b36143b6d6dbe327`  
		Last Modified: Wed, 05 Sep 2018 08:55:51 GMT  
		Size: 609.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
