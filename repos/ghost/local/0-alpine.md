# `ghost:0.11.13-alpine`

## Docker Metadata

- Image ID: `sha256:3b4338e27ee1017e37720965cc8003a45dd52cf752090235e7f022d1f1c12e88`
- Created: `2018-09-05T08:52:39.815151256Z`
- Virtual Size: ~ 174.08 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["npm","start"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=6.14.4`
  - `YARN_VERSION=1.6.0`
  - `GHOST_SOURCE=/usr/src/ghost`
  - `GHOST_VERSION=0.11.13`
  - `GHOST_CONTENT=/var/lib/ghost`
