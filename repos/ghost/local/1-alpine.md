# `ghost:1.25.5-alpine`

## Docker Metadata

- Image ID: `sha256:90d61854d14fe9a768db0e776f517032ba57dcb68e49c2cb49584efe1e2a01d2`
- Created: `2018-09-05T08:49:25.755117567Z`
- Virtual Size: ~ 421.33 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["node","current/index.js"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/ghost/current/node_modules/knex-migrator/bin`
  - `NODE_VERSION=8.11.4`
  - `YARN_VERSION=1.6.0`
  - `NODE_ENV=production`
  - `GHOST_CLI_VERSION=1.9.1`
  - `GHOST_INSTALL=/var/lib/ghost`
  - `GHOST_CONTENT=/var/lib/ghost/content`
  - `GHOST_VERSION=1.25.5`
