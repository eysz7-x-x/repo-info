# `ghost:2.0.3-alpine`

## Docker Metadata

- Image ID: `sha256:dc6284121256e0298350b2d4810444d3154187aa2edead4403bbbc2f752f90a7`
- Created: `2018-09-05T08:46:52.119118591Z`
- Virtual Size: ~ 424.82 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["node","current/index.js"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=8.11.4`
  - `YARN_VERSION=1.6.0`
  - `NODE_ENV=production`
  - `GHOST_CLI_VERSION=1.9.1`
  - `GHOST_INSTALL=/var/lib/ghost`
  - `GHOST_CONTENT=/var/lib/ghost/content`
  - `GHOST_VERSION=2.0.3`
