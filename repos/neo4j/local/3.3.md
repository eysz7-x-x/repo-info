# `neo4j:3.3.6`

## Docker Metadata

- Image ID: `sha256:2ac4504aac9b12c84348dfde0f14e048967e4447cb5f6bbbc7a592771a65fa4d`
- Created: `2018-09-05T13:19:12.839417611Z`
- Virtual Size: ~ 191.40 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/sbin/tini","-g","--","/docker-entrypoint.sh"]`
- Command: `["neo4j"]`
- Environment:
  - `PATH=/var/lib/neo4j/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `NEO4J_SHA256=e6d3c28621eaac7306c706fb22d8336d150d2b37218ca89f5ed5d15fd1676d26`
  - `NEO4J_TARBALL=neo4j-community-3.3.6-unix.tar.gz`
  - `NEO4J_EDITION=community`
