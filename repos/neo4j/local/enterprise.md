# `neo4j:3.4.7-enterprise`

## Docker Metadata

- Image ID: `sha256:21276e1c935aba8b493fd066a8bed8736fe500c76b99c68d57ef8bb3cf3e91a0`
- Created: `2018-09-05T13:13:32.190274437Z`
- Virtual Size: ~ 209.35 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/sbin/tini","-g","--","/docker-entrypoint.sh"]`
- Command: `["neo4j"]`
- Environment:
  - `PATH=/var/lib/neo4j/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk/jre`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `NEO4J_SHA256=dd4f8020fbf8cb0c2bd245c18596abfd7c826948fd726bb4971039e949462bfe`
  - `NEO4J_TARBALL=neo4j-enterprise-3.4.7-unix.tar.gz`
  - `NEO4J_EDITION=enterprise`
