## `haxe:3.2.1-alpine3.8`

```console
$ docker pull haxe@sha256:1fd9511c2d7d3bfe5a8f54464ba55253c7f9ef69b032014b3eac166974b6c827
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64
	-	linux; arm64 variant v8

### `haxe:3.2.1-alpine3.8` - linux; amd64

```console
$ docker pull haxe@sha256:95b70d31552b33449f1f036d261911b8f11c1b6780605406a60ea3b219aba1de
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **23.3 MB (23262203 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:14680eb6a5fffc600f89c985b9225ec84ded40a5793478f88d08cb255b996f2c`
-	Default Command: `["haxe"]`

```dockerfile
# Fri, 06 Jul 2018 14:14:06 GMT
ADD file:25f61d70254b9807a40cd3e8d820f6a5ec0e1e596de04e325f6a33810393e95a in / 
# Fri, 06 Jul 2018 14:14:06 GMT
CMD ["/bin/sh"]
# Wed, 05 Sep 2018 03:31:38 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Wed, 05 Sep 2018 09:05:05 GMT
RUN apk add --no-cache ca-certificates
# Wed, 05 Sep 2018 09:05:05 GMT
ENV NEKO_VERSION=2.2.0
# Wed, 05 Sep 2018 09:18:19 GMT
ENV HAXE_VERSION=3.2.1
# Wed, 05 Sep 2018 09:18:19 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Wed, 05 Sep 2018 09:19:05 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		libressl 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-2-0/neko-2.2.0.tar.gz" 	&& echo "cf101ca05db6cb673504efe217d8ed7ab5638f30e12c5e3095f06fa0d43f64e3 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.2.1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& apk add --no-cache --virtual .build-deps 		pcre-dev 		zlib-dev 		make 				ocaml 		camlp4 		ocaml-camlp4-dev 				&& OCAMLPARAM=safe-string=0,_ make all tools 		&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .python-rundeps $runDeps 	&& apk del .build-deps 	&& apk del .fetch-deps 		&& rm -rf /usr/src/neko /usr/src/haxe
# Wed, 05 Sep 2018 09:19:06 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:8e3ba11ec2a2b39ab372c60c16b421536e50e5ce64a0bc81765c2e38381bcff6`  
		Last Modified: Fri, 06 Jul 2018 04:15:58 GMT  
		Size: 2.2 MB (2206542 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:6f2824f375da66ee37ced70fea1ca7c7a688d98db2c20d22dbad9ee92f1e49f1`  
		Last Modified: Wed, 05 Sep 2018 10:13:44 GMT  
		Size: 309.0 KB (308996 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b614026e7500ce8b09459bd3fa3efdfeb75fbcdd0964ac71e5714c5a1515dccc`  
		Last Modified: Wed, 05 Sep 2018 10:17:22 GMT  
		Size: 20.7 MB (20746665 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip

### `haxe:3.2.1-alpine3.8` - linux; arm64 variant v8

```console
$ docker pull haxe@sha256:8eef70ca65b9a9db3bce0941c609e5f448b99c201cc0101d6bfc32a58e853e83
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **22.9 MB (22865414 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:4a3ce974e36423b3e2be8730eed0b9208e27ef42be48559be683cab22d963afa`
-	Default Command: `["haxe"]`

```dockerfile
# Fri, 06 Jul 2018 08:41:03 GMT
ADD file:199a5a48bddabaf1f649f58f3b17c323a1aa1a50e939dfdea3542e4041e91b7b in / 
# Fri, 06 Jul 2018 08:41:03 GMT
COPY file:0f1d36dd7d8d53613b275660a88c5bf9b608ea8aa73a8054cb8bdbd73fd971ac in /etc/localtime 
# Fri, 06 Jul 2018 08:41:04 GMT
CMD ["/bin/sh"]
# Tue, 17 Jul 2018 11:50:00 GMT
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Wed, 25 Jul 2018 08:40:09 GMT
RUN apk add --no-cache ca-certificates
# Wed, 25 Jul 2018 08:40:22 GMT
ENV NEKO_VERSION=2.2.0
# Wed, 25 Jul 2018 08:59:01 GMT
ENV HAXE_VERSION=3.2.1
# Wed, 25 Jul 2018 08:59:02 GMT
ENV HAXE_STD_PATH=/usr/local/share/haxe/std
# Wed, 25 Jul 2018 09:00:56 GMT
RUN set -ex 	&& apk add --no-cache --virtual .fetch-deps 		libressl 		tar 		git 		&& wget -O neko.tar.gz "https://github.com/HaxeFoundation/neko/archive/v2-2-0/neko-2.2.0.tar.gz" 	&& echo "cf101ca05db6cb673504efe217d8ed7ab5638f30e12c5e3095f06fa0d43f64e3 *neko.tar.gz" | sha256sum -c - 	&& mkdir -p /usr/src/neko 	&& tar -xC /usr/src/neko --strip-components=1 -f neko.tar.gz 	&& rm neko.tar.gz 	&& apk add --no-cache --virtual .build-deps 		apache2-dev 		cmake 		gc-dev 		gcc 		gtk+2.0-dev 		libc-dev 		linux-headers 		mariadb-dev 		mbedtls-dev 		ninja 		sqlite-dev 	&& cd /usr/src/neko 	&& cmake -GNinja -DNEKO_JIT_DISABLE=ON -DRELOCATABLE=OFF -DRUN_LDCONFIG=OFF . 	&& ninja 	&& ninja install 		&& git clone --recursive --depth 1 --branch 3.2.1 "https://github.com/HaxeFoundation/haxe.git" /usr/src/haxe 	&& cd /usr/src/haxe 	&& apk add --no-cache --virtual .build-deps 		pcre-dev 		zlib-dev 		make 				ocaml 		camlp4 		ocaml-camlp4-dev 				&& OCAMLPARAM=safe-string=0,_ make all tools 		&& mkdir -p /usr/local/bin 	&& cp haxe haxelib /usr/local/bin 	&& mkdir -p $HAXE_STD_PATH 	&& cp -r std/* $HAXE_STD_PATH 	&& mkdir -p /haxelib 	&& cd / && haxelib setup /haxelib 		&& runDeps="$( 		scanelf --needed --nobanner --recursive /usr/local 			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' 			| sort -u 			| xargs -r apk info --installed 			| sort -u 	)" 	&& apk add --virtual .python-rundeps $runDeps 	&& apk del .build-deps 	&& apk del .fetch-deps 		&& rm -rf /usr/src/neko /usr/src/haxe
# Wed, 25 Jul 2018 09:00:58 GMT
CMD ["haxe"]
```

-	Layers:
	-	`sha256:47e04371c99027fae42871b720fdc6cdddcb65062bfa05f0c3bb0a594cb5bbbd`  
		Last Modified: Wed, 27 Jun 2018 19:15:35 GMT  
		Size: 2.1 MB (2099514 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:b4103359e1ecd9a7253d8b8a041d4e81db1ff4a5e1950bc0e02305d221c9e6c2`  
		Last Modified: Fri, 06 Jul 2018 08:42:09 GMT  
		Size: 176.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:0b204bacd884e3d00d399211a215d446d1eb076c82b6d85b7b4fafa0126e1af9`  
		Last Modified: Wed, 25 Jul 2018 10:12:33 GMT  
		Size: 308.5 KB (308527 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:9b2f894017573956c65bb8849bd3fdd899138ff5255be9eabc125de25ffe5ddb`  
		Last Modified: Wed, 25 Jul 2018 10:19:50 GMT  
		Size: 20.5 MB (20457197 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
