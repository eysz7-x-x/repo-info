# `haxe:4.0.0-preview.4-alpine3.7`

## Docker Metadata

- Image ID: `sha256:66911879fe9b3b887606db7e5038aa10b66728a4df18cac1ac25b4474d76a0cb`
- Created: `2018-09-05T09:59:46.679133961Z`
- Virtual Size: ~ 74.24 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=4.0.0-preview.4`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
