# `haxe:4.0.0-preview.4-alpine3.8`

## Docker Metadata

- Image ID: `sha256:45eef87bd8142a5e3b46f181cfca94ed3ad835baeee96b2d5db47191bd21e716`
- Created: `2018-09-05T09:47:09.868995339Z`
- Virtual Size: ~ 71.10 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=4.0.0-preview.4`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
