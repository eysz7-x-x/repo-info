# `haxe:3.3.0-rc.1-alpine3.8`

## Docker Metadata

- Image ID: `sha256:dcc6abc9d3f5ccc38b32e5119eed9a5a694bdfb536d44f5f0032764adaf9c38e`
- Created: `2018-09-05T09:13:00.047660005Z`
- Virtual Size: ~ 66.72 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["haxe"]`
- Environment:
  - `PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NEKO_VERSION=2.2.0`
  - `HAXE_VERSION=3.3.0-rc.1`
  - `HAXE_STD_PATH=/usr/local/share/haxe/std`
