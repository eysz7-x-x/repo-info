# `kapacitor:1.5-alpine`

## Docker Metadata

- Image ID: `sha256:f55aed6a090d23d6230ac60a515ca390e62ef161b2371b1590bae3953d2d461b`
- Created: `2018-09-05T11:04:17.554944041Z`
- Virtual Size: ~ 63.49 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["kapacitord"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `KAPACITOR_VERSION=1.5.1`
