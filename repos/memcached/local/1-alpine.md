# `memcached:1.5.10-alpine`

## Docker Metadata

- Image ID: `sha256:7f73a9e6468aecc7672624543dd8b437b6b2d6b1c7409b46a9f6ac801df52167`
- Created: `2018-09-05T01:18:15.509792798Z`
- Virtual Size: ~ 8.97 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["memcached"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `MEMCACHED_VERSION=1.5.10`
  - `MEMCACHED_SHA1=fff9351b360a09497cd805d3e4c1ffbe860d067d`
