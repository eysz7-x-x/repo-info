## `redis:5.0-rc-32bit-stretch`

```console
$ docker pull redis@sha256:d7865e44b0f096f9e28a3c2036f1311667f2c10c94cb5b85afa8c84b09c2ef7a
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `redis:5.0-rc-32bit-stretch` - linux; amd64

```console
$ docker pull redis@sha256:c3dcf4ff5b660f08aa2718eab8ce28185a24eaca0740d088cd933d8e1f8bc12e
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **39.3 MB (39345569 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:96aafc2c1d0fd3b529d18eb968664543e09d73a902b338a8fcd12e4af78e25aa`
-	Entrypoint: `["docker-entrypoint.sh"]`
-	Default Command: `["redis-server"]`

```dockerfile
# Tue, 04 Sep 2018 21:21:34 GMT
ADD file:e6ca98733431f75e97eb09758ba64065d213d51bd2070a95cf15f2ff5adccfc4 in / 
# Tue, 04 Sep 2018 21:21:34 GMT
CMD ["bash"]
# Wed, 05 Sep 2018 08:23:30 GMT
RUN groupadd -r redis && useradd -r -g redis redis
# Wed, 05 Sep 2018 08:23:30 GMT
ENV GOSU_VERSION=1.10
# Wed, 05 Sep 2018 08:23:44 GMT
RUN set -ex; 		fetchDeps=" 		ca-certificates 		dirmngr 		gnupg 		wget 	"; 	apt-get update; 	apt-get install -y --no-install-recommends $fetchDeps; 	rm -rf /var/lib/apt/lists/*; 		dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; 	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; 	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; 	export GNUPGHOME="$(mktemp -d)"; 	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; 	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; 	gpgconf --kill all; 	rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; 	chmod +x /usr/local/bin/gosu; 	gosu nobody true; 		apt-get purge -y --auto-remove $fetchDeps
# Wed, 05 Sep 2018 08:23:44 GMT
ENV REDIS_VERSION=5.0-rc4
# Wed, 05 Sep 2018 08:23:45 GMT
ENV REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-5.0-rc4.tar.gz
# Wed, 05 Sep 2018 08:23:45 GMT
ENV REDIS_DOWNLOAD_SHA=bfc7a27d3ba990e154e5b56484061f01962d40b7c77b520ed7a940914b267cec
# Wed, 05 Sep 2018 08:24:50 GMT
RUN apt-get update && apt-get install -y --no-install-recommends 		libc6-i386 	&& rm -rf /var/lib/apt/lists/*
# Wed, 05 Sep 2018 08:26:24 GMT
RUN set -ex; 		buildDeps=' 		ca-certificates 		wget 				gcc 		gcc-multilib 		libc6-dev-i386 		make 	'; 	apt-get update; 	apt-get install -y $buildDeps --no-install-recommends; 	rm -rf /var/lib/apt/lists/*; 		wget -O redis.tar.gz "$REDIS_DOWNLOAD_URL"; 	echo "$REDIS_DOWNLOAD_SHA *redis.tar.gz" | sha256sum -c -; 	mkdir -p /usr/src/redis; 	tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1; 	rm redis.tar.gz; 		grep -q '^#define CONFIG_DEFAULT_PROTECTED_MODE 1$' /usr/src/redis/src/server.h; 	sed -ri 's!^(#define CONFIG_DEFAULT_PROTECTED_MODE) 1$!\1 0!' /usr/src/redis/src/server.h; 	grep -q '^#define CONFIG_DEFAULT_PROTECTED_MODE 0$' /usr/src/redis/src/server.h; 		make -C /usr/src/redis -j "$(nproc)" 32bit; 	make -C /usr/src/redis install; 		rm -r /usr/src/redis; 		apt-get purge -y --auto-remove $buildDeps
# Wed, 05 Sep 2018 08:26:25 GMT
RUN mkdir /data && chown redis:redis /data
# Wed, 05 Sep 2018 08:26:25 GMT
VOLUME [/data]
# Wed, 05 Sep 2018 08:26:26 GMT
WORKDIR /data
# Wed, 05 Sep 2018 08:26:26 GMT
COPY file:9c29fbe8374a97f9c2d953c9c8b7224554607eeb7a610a930844f2bec678265c in /usr/local/bin/ 
# Wed, 05 Sep 2018 08:26:26 GMT
ENTRYPOINT ["docker-entrypoint.sh"]
# Wed, 05 Sep 2018 08:26:26 GMT
EXPOSE 6379/tcp
# Wed, 05 Sep 2018 08:26:27 GMT
CMD ["redis-server"]
```

-	Layers:
	-	`sha256:802b00ed6f79f48e6a5f44ecbcaf43563d6077aaecb565eee1dfc615c0b18c00`  
		Last Modified: Tue, 04 Sep 2018 21:25:45 GMT  
		Size: 22.5 MB (22485965 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8b4a21f633de1f1a9613f4a2140e0f5825843ee45f94a1615e2bb45aa7b0fd1d`  
		Last Modified: Wed, 05 Sep 2018 08:34:15 GMT  
		Size: 1.7 KB (1741 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:92e244f8ff147cc7df74f0c2ab84f12fbeb7822673c6078127c26e8e5501a956`  
		Last Modified: Wed, 05 Sep 2018 08:34:15 GMT  
		Size: 941.3 KB (941310 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f7b6de69fa7a1c443495a5ddc636b902bb774c07e3e52937131409799c9ca5d9`  
		Last Modified: Wed, 05 Sep 2018 08:34:34 GMT  
		Size: 4.8 MB (4848320 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f988d1fd995ed52cf947935435ee4d84da574a5f3f9a6621760792ff73e3caf7`  
		Last Modified: Wed, 05 Sep 2018 08:34:36 GMT  
		Size: 11.1 MB (11067726 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:595c4907b4757a942f43a1f461f83f4e233b5f57a2565f23b1365c40963a0e18`  
		Last Modified: Wed, 05 Sep 2018 08:34:33 GMT  
		Size: 98.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ac416d43024ae5352b96dbadef1ac2637a8ea556f0633791d48dbfcee71b3fef`  
		Last Modified: Wed, 05 Sep 2018 08:34:33 GMT  
		Size: 409.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
