## `arangodb:latest`

```console
$ docker pull arangodb@sha256:84989d06e1498b29781331f5f221239a33d71aadb9fd9f4e9945297257c60894
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `arangodb:latest` - linux; amd64

```console
$ docker pull arangodb@sha256:866da1d84017fc88550dc3e5780035621ff5b1bc17b9dc92a9e952cd2ffb9485
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **117.5 MB (117452069 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:d9112ac0bd8b07cf26336ab296c79fe398825d535e8c46e840b52aeebdb79a1a`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["arangod"]`

```dockerfile
# Tue, 04 Sep 2018 21:21:24 GMT
ADD file:58d5c21fcabcf1eec94e8676a3b1e51c5fdc2db5c7b866a761f907fa30ede4d8 in / 
# Tue, 04 Sep 2018 21:21:24 GMT
CMD ["bash"]
# Tue, 04 Sep 2018 22:57:53 GMT
MAINTAINER Frank Celler <info@arangodb.com>
# Tue, 04 Sep 2018 22:57:53 GMT
ENV ARCHITECTURE=amd64
# Tue, 04 Sep 2018 22:57:53 GMT
ENV DEB_PACKAGE_VERSION=1
# Tue, 04 Sep 2018 22:58:38 GMT
ENV ARANGO_VERSION=3.3.14
# Tue, 04 Sep 2018 22:58:38 GMT
ENV ARANGO_URL=https://download.arangodb.com/arangodb33/Debian_9.0
# Tue, 04 Sep 2018 22:58:38 GMT
ENV ARANGO_PACKAGE=arangodb3-3.3.14-1_amd64.deb
# Tue, 04 Sep 2018 22:58:38 GMT
ENV ARANGO_PACKAGE_URL=https://download.arangodb.com/arangodb33/Debian_9.0/amd64/arangodb3-3.3.14-1_amd64.deb
# Tue, 04 Sep 2018 22:58:39 GMT
ENV ARANGO_SIGNATURE_URL=https://download.arangodb.com/arangodb33/Debian_9.0/amd64/arangodb3-3.3.14-1_amd64.deb.asc
# Tue, 04 Sep 2018 22:58:44 GMT
RUN apt-get update &&     apt-get install -y --no-install-recommends gpg dirmngr     &&     rm -rf /var/lib/apt/lists/*
# Tue, 04 Sep 2018 22:58:48 GMT
RUN gpg --keyserver hkps://hkps.pool.sks-keyservers.net --recv-keys CD8CB0F1E0AD5B52E93F41E7EA93F5E56E751E9B
# Tue, 04 Sep 2018 22:58:56 GMT
RUN apt-get update &&     apt-get install -y --no-install-recommends         libjemalloc1         ca-certificates         pwgen         curl     &&     rm -rf /var/lib/apt/lists/*
# Tue, 04 Sep 2018 22:58:57 GMT
RUN mkdir /docker-entrypoint-initdb.d
# Tue, 04 Sep 2018 22:59:11 GMT
RUN curl --fail -O ${ARANGO_SIGNATURE_URL} &&           curl --fail -O ${ARANGO_PACKAGE_URL} &&             gpg --verify ${ARANGO_PACKAGE}.asc &&     (echo arangodb3 arangodb3/password password test | debconf-set-selections) &&     (echo arangodb3 arangodb3/password_again password test | debconf-set-selections) &&     DEBIAN_FRONTEND="noninteractive" dpkg -i ${ARANGO_PACKAGE} &&     rm -rf /var/lib/arangodb3/* &&     sed -ri         -e 's!127\.0\.0\.1!0.0.0.0!g'         -e 's!^(file\s*=).*!\1 -!'         -e 's!^\s*uid\s*=.*!!'         /etc/arangodb3/arangod.conf     && chgrp 0 /var/lib/arangodb3 /var/lib/arangodb3-apps     && chmod 775 /var/lib/arangodb3 /var/lib/arangodb3-apps     &&     rm -f ${ARANGO_PACKAGE}*
# Tue, 04 Sep 2018 22:59:11 GMT
VOLUME [/var/lib/arangodb3 /var/lib/arangodb3-apps]
# Tue, 04 Sep 2018 22:59:12 GMT
COPY file:a1c9828bd2bbf6262810c7ebdad273e47b19b1e40fb23c533431934c89329a8f in /entrypoint.sh 
# Tue, 04 Sep 2018 22:59:12 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Tue, 04 Sep 2018 22:59:12 GMT
EXPOSE 8529/tcp
# Tue, 04 Sep 2018 22:59:12 GMT
CMD ["arangod"]
```

-	Layers:
	-	`sha256:05d1a5232b461a4b35424129580054caa878cd56f100e34282510bd4b4082e4d`  
		Last Modified: Tue, 04 Sep 2018 21:25:27 GMT  
		Size: 45.3 MB (45310060 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:f3e404de5952570aa3b0c0226c9e03c6dc45559b836ea3b124e5e7393e92da75`  
		Last Modified: Tue, 04 Sep 2018 22:59:59 GMT  
		Size: 6.6 MB (6561915 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:8adbad601f68552d65d645f3a1e4cfa6b8abf1c7399d0e4a6ceff5e551bd8aa6`  
		Last Modified: Tue, 04 Sep 2018 22:59:57 GMT  
		Size: 3.5 KB (3469 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:ca9a9ce969b19d330aca14730f5127f6686a7bdfd7606335856974b8db115ae1`  
		Last Modified: Tue, 04 Sep 2018 22:59:59 GMT  
		Size: 7.3 MB (7320769 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:7d60557512a5e84187e034863fd3f51fea9f5798f36a6624afeba8da873e99b1`  
		Last Modified: Tue, 04 Sep 2018 22:59:57 GMT  
		Size: 115.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:1616d2b1b6d05753d4f566b90fb3bb10fa98aaa533b1671b2b3cef936183b772`  
		Last Modified: Tue, 04 Sep 2018 23:00:09 GMT  
		Size: 58.3 MB (58253943 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:762ed0fd085c871d228947b639390a9ca09cd73026929b34db68a0ec7408fb35`  
		Last Modified: Tue, 04 Sep 2018 22:59:57 GMT  
		Size: 1.8 KB (1798 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
