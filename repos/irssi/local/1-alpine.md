# `irssi:1.1.1-alpine`

## Docker Metadata

- Image ID: `sha256:6dd7a1b7b75cbd15736624db1c27cbb4ef2e49ceeb3802a985052aedf04c7a83`
- Created: `2018-09-05T00:11:27.246983618Z`
- Virtual Size: ~ 61.55 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["irssi"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `HOME=/home/user`
  - `LANG=C.UTF-8`
  - `IRSSI_VERSION=1.1.1`
