# `ibmjava:8-sfj-alpine`

## Docker Metadata

- Image ID: `sha256:0cb83ef8138289ab5f87c4eb8afad1540719eaedb3fa3b1916aa57c857bb1853`
- Created: `2018-09-05T21:22:49.230524471Z`
- Virtual Size: ~ 109.64 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/opt/ibm/java/jre/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `JAVA_VERSION=1.8.0_sr5fp21`
  - `JAVA_HOME=/opt/ibm/java/jre`
  - `IBM_JAVA_OPTIONS=-XX:+UseContainerSupport`
