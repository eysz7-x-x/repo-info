# `elixir:1.6.6-alpine`

## Docker Metadata

- Image ID: `sha256:e9a46db6bcb90a4906fca618d323b3a135232002093a4f0c51ceeb049e5de403`
- Created: `2018-08-31T18:58:14.766329354Z`
- Virtual Size: ~ 85.09 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["iex"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=20.3.8.8`
  - `ELIXIR_VERSION=v1.6.6`
  - `LANG=C.UTF-8`
