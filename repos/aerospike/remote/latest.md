## `aerospike:latest`

```console
$ docker pull aerospike@sha256:2198cd2861a64ee65404e8d11760baa2d7fb0046e37c8b0bf933d780c57e3fb0
```

-	Manifest MIME: `application/vnd.docker.distribution.manifest.list.v2+json`
-	Platforms:
	-	linux; amd64

### `aerospike:latest` - linux; amd64

```console
$ docker pull aerospike@sha256:197599448df3867366310e27940123d2260d7457241a50c3f801652376aa58ce
```

-	Docker Version: 17.06.2-ce
-	Manifest MIME: `application/vnd.docker.distribution.manifest.v2+json`
-	Total Size: **48.8 MB (48759739 bytes)**  
	(compressed transfer size, not on-disk size)
-	Image ID: `sha256:26e5f330d91b56c97ff978c5ad59938be042c6e2cf9d63ae60033870bf696223`
-	Entrypoint: `["\/entrypoint.sh"]`
-	Default Command: `["asd"]`

```dockerfile
# Tue, 04 Sep 2018 21:21:34 GMT
ADD file:e6ca98733431f75e97eb09758ba64065d213d51bd2070a95cf15f2ff5adccfc4 in / 
# Tue, 04 Sep 2018 21:21:34 GMT
CMD ["bash"]
# Tue, 04 Sep 2018 23:00:33 GMT
ENV AEROSPIKE_VERSION=4.3.0.6
# Tue, 04 Sep 2018 23:00:34 GMT
ENV AEROSPIKE_SHA256=e8f898211a5fd01c14da8ae1f71468d26cb7d7bac04d9f4674ee61383e8f5de6
# Tue, 04 Sep 2018 23:00:58 GMT
RUN apt-get update -y   && apt-get install -y wget python lua5.2 gettext-base   && wget "https://www.aerospike.com/artifacts/aerospike-server-community/${AEROSPIKE_VERSION}/aerospike-server-community-${AEROSPIKE_VERSION}-debian9.tgz" -O aerospike-server.tgz   && echo "$AEROSPIKE_SHA256 *aerospike-server.tgz" | sha256sum -c -   && mkdir aerospike   && tar xzf aerospike-server.tgz --strip-components=1 -C aerospike   && dpkg -i aerospike/aerospike-server-*.deb   && dpkg -i aerospike/aerospike-tools-*.deb   && mkdir -p /var/log/aerospike/   && mkdir -p /var/run/aerospike/   && rm -rf aerospike-server.tgz aerospike /var/lib/apt/lists/*   && rm -rf /opt/aerospike/lib/java   && dpkg -r wget ca-certificates openssl xz-utils  && dpkg --purge wget ca-certificates openssl xz-utils  && apt-get purge -y   && apt autoremove -y
# Tue, 04 Sep 2018 23:00:58 GMT
COPY file:92f154ac5768cc66c29bd7ca3d00a0fe0ae8d08f1d309fdcda8bf66d4c73cadd in /etc/aerospike/aerospike.template.conf 
# Tue, 04 Sep 2018 23:00:58 GMT
COPY file:7eece3188902a85a78ecb96d2ec561fce45fa1728926bc66f3903d6955630907 in /entrypoint.sh 
# Tue, 04 Sep 2018 23:00:58 GMT
VOLUME [/opt/aerospike/data]
# Tue, 04 Sep 2018 23:00:59 GMT
EXPOSE 3000/tcp 3001/tcp 3002/tcp 3003/tcp
# Tue, 04 Sep 2018 23:00:59 GMT
ENTRYPOINT ["/entrypoint.sh"]
# Tue, 04 Sep 2018 23:00:59 GMT
CMD ["asd"]
```

-	Layers:
	-	`sha256:802b00ed6f79f48e6a5f44ecbcaf43563d6077aaecb565eee1dfc615c0b18c00`  
		Last Modified: Tue, 04 Sep 2018 21:25:45 GMT  
		Size: 22.5 MB (22485965 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:3188eac7dbd8bc54efd73f2b45295288f4097abaefa2a2b6d8d969170b9dcdb1`  
		Last Modified: Tue, 04 Sep 2018 23:01:16 GMT  
		Size: 26.3 MB (26271782 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:65db6c4b1302f1744e1b79bc53146509c41d94bdc8c7f54c0b9e915c7261604a`  
		Last Modified: Tue, 04 Sep 2018 23:01:10 GMT  
		Size: 1.1 KB (1109 bytes)  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
	-	`sha256:77e7a246dfea6c846e2c6007b911fd01c882c0a82088099c01b719016a9e2309`  
		Last Modified: Tue, 04 Sep 2018 23:01:15 GMT  
		Size: 883.0 B  
		MIME: application/vnd.docker.image.rootfs.diff.tar.gzip
