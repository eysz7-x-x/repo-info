# `clojure:lein-2.8.1-alpine-onbuild`

## Docker Metadata

- Image ID: `sha256:eb0735f42eec93805892089031daf74edd0ddf9a71b797305235cd63744af4c3`
- Created: `2018-09-05T12:07:19.482623459Z`
- Virtual Size: ~ 140.41 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["lein","run"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin:/usr/local/bin/`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `LEIN_VERSION=2.8.1`
  - `LEIN_INSTALL=/usr/local/bin/`
  - `LEIN_ROOT=1`
