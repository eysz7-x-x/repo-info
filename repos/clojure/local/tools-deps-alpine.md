# `clojure:tools-deps-1.9.0.391-alpine`

## Docker Metadata

- Image ID: `sha256:b5143caf6c084be2c547c9d536452a9f08e6935bbde36f5e59b790f2665bbad1`
- Created: `2018-09-05T12:11:44.068018176Z`
- Virtual Size: ~ 133.89 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["/bin/sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `CLOJURE_VERSION=1.9.0.391`
