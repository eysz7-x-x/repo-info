# `chronograf:1.6-alpine`

## Docker Metadata

- Image ID: `sha256:32f9942e0f7a0742592b7e2993a6cc89057d66789a8af881f62ad082eeb10754`
- Created: `2018-09-04T23:05:19.206143331Z`
- Virtual Size: ~ 42.70 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["/entrypoint.sh"]`
- Command: `["chronograf"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `CHRONOGRAF_VERSION=1.6.1`
