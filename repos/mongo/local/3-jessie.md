# `mongo:3.6.7-jessie`

## Docker Metadata

- Image ID: `sha256:b539e1432629f72bef1b053f0942c23852e0ca47a25e02eeb79107b68cf89970`
- Created: `2018-09-05T01:50:13.006844127Z`
- Virtual Size: ~ 367.84 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.10`
  - `JSYAML_VERSION=3.10.0`
  - `GPG_KEYS=2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5`
  - `MONGO_PACKAGE=mongodb-org`
  - `MONGO_REPO=repo.mongodb.org`
  - `MONGO_MAJOR=3.6`
  - `MONGO_VERSION=3.6.7`
