# `mongo:4.0.1-xenial`

## Docker Metadata

- Image ID: `sha256:ae68bfa1087debc664fcc1c2c1a665fcc2700a752dc51e80154b6b9d9817f2ab`
- Created: `2018-09-05T23:00:58.210538685Z`
- Virtual Size: ~ 380.29 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.10`
  - `JSYAML_VERSION=3.10.0`
  - `GPG_KEYS=9DA31620334BD75D9DCB49F368818C72E52529D4`
  - `MONGO_PACKAGE=mongodb-org`
  - `MONGO_REPO=repo.mongodb.org`
  - `MONGO_MAJOR=4.0`
  - `MONGO_VERSION=4.0.1`
