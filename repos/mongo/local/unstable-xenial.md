# `mongo:4.1.2-xenial`

## Docker Metadata

- Image ID: `sha256:837f056961bfdcfc592db03759a4b3f7db4f1bebafe354f0589a480c46726ffd`
- Created: `2018-09-05T23:01:31.023223236Z`
- Virtual Size: ~ 385.69 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["mongod"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `GOSU_VERSION=1.10`
  - `JSYAML_VERSION=3.10.0`
  - `GPG_KEYS=E162F504A20CDF15827F718D4B7C549A058F8B6B`
  - `MONGO_PACKAGE=mongodb-org-unstable`
  - `MONGO_REPO=repo.mongodb.org`
  - `MONGO_MAJOR=4.1`
  - `MONGO_VERSION=4.1.2`
