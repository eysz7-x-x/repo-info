# `orientdb:3.0.6`

## Docker Metadata

- Image ID: `sha256:341ff6a0990fd35055c2491442298f1aad1b4630943f6c87bfc5d5340616c967`
- Created: `2018-09-05T15:42:13.022661752Z`
- Virtual Size: ~ 145.59 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["server.sh"]`
- Environment:
  - `PATH=/orientdb/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin`
  - `LANG=C.UTF-8`
  - `JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk`
  - `JAVA_VERSION=8u171`
  - `JAVA_ALPINE_VERSION=8.171.11-r0`
  - `ORIENTDB_VERSION=3.0.6`
  - `ORIENTDB_DOWNLOAD_MD5=90d88e4d3e3932c1a99ba7155ec52b09`
  - `ORIENTDB_DOWNLOAD_SHA1=7b0b6a07638b93cbaf53c7dafb2a9e6c451dc135`
  - `ORIENTDB_DOWNLOAD_URL=http://central.maven.org/maven2/com/orientechnologies/orientdb-community/3.0.6/orientdb-community-3.0.6.tar.gz`
