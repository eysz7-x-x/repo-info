# `docker:18.09.0-ce-tp4`

## Docker Metadata

- Image ID: `sha256:228a322128e95577a3687465669d5d89e32b4a3b4d25d034c9f892dbfb354a01`
- Created: `2018-08-30T21:47:11.84235692Z`
- Virtual Size: ~ 166.76 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=test`
  - `DOCKER_VERSION=18.09.0-ce-tp4`
