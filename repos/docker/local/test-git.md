# `docker:18.09.0-ce-tp4-git`

## Docker Metadata

- Image ID: `sha256:d28f5382c035d9b164e31b2405031a639cab43f76280a9d59a6b227326c65cf1`
- Created: `2018-08-30T21:47:21.98536739Z`
- Virtual Size: ~ 184.24 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=test`
  - `DOCKER_VERSION=18.09.0-ce-tp4`
