# `docker:18.06.1-ce`

## Docker Metadata

- Image ID: `sha256:9797f6e6a0689dffe3cc376ce6de0e938aa1099839524302014ab1881cab8dd3`
- Created: `2018-08-30T21:47:31.006678541Z`
- Virtual Size: ~ 153.33 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["docker-entrypoint.sh"]`
- Command: `["sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=stable`
  - `DOCKER_VERSION=18.06.1-ce`
