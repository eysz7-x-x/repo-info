# `docker:18.06.1-ce-dind`

## Docker Metadata

- Image ID: `sha256:5420f3e284675454f16b449ba8004f2dd921bd8a5c23209bfd2449a303767061`
- Created: `2018-08-30T21:47:37.5388161Z`
- Virtual Size: ~ 164.87 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Entrypoint: `["dockerd-entrypoint.sh"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `DOCKER_CHANNEL=stable`
  - `DOCKER_VERSION=18.06.1-ce`
  - `DIND_COMMIT=52379fa76dee07ca038624d639d9e14f4fb719ff`
