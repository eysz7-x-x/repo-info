# `node:6.14.4-alpine`

## Docker Metadata

- Image ID: `sha256:f4853a77cc117fd1475ab8c230fb4ef670522c37a07701764987fbd6f5f86d09`
- Created: `2018-09-04T22:21:46.955556444Z`
- Virtual Size: ~ 55.24 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["node"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=6.14.4`
  - `YARN_VERSION=1.6.0`
