# `node:10.9.0-alpine`

## Docker Metadata

- Image ID: `sha256:df2d34f007a1b8faeff432f21fc0839e135ef567d73aac5bae9f0bcf5007f6ac`
- Created: `2018-09-04T22:49:17.751520645Z`
- Virtual Size: ~ 70.57 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["node"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `NODE_VERSION=10.9.0`
  - `YARN_VERSION=1.9.2`
