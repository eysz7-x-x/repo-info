# `erlang:20.3.8.8-alpine`

## Docker Metadata

- Image ID: `sha256:e267d244aabcaa48e0a33693bd0ea4df5eae66695cfd4bc2d3d624aef105d4e7`
- Created: `2018-08-31T17:44:50.683194584Z`
- Virtual Size: ~ 76.47 Mb  
  (total size of all layers on-disk)
- Arch: `linux`/`amd64`
- Command: `["erl"]`
- Environment:
  - `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`
  - `OTP_VERSION=20.3.8.8`
